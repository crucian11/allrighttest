package ua.`in`.karas.allrighttest.data

import androidx.lifecycle.LiveData
import ua.`in`.karas.allrighttest.data.cache.PicMatchCacheSource
import ua.`in`.karas.allrighttest.data.model.*
import ua.`in`.karas.allrighttest.data.util.map
import java.lang.IllegalArgumentException

open class TestTaskRepo(private val picMatchCacheSource: PicMatchCacheSource) {

    open fun getTaskTask(taskId: Long): LiveData<Test> =
            picMatchCacheSource.getPicMatchTest(taskId).map { it }

    // Tend to move all of business logic to the repository
    open fun checkAnswers(answeredTask: TestAnswered): LiveData<TestChecked> =
            picMatchCacheSource.getPicMatchTestAnswered(answeredTask.id)
                .map { properlyAnsweredTask ->

                    if (answeredTask !is PicMatchTestAnswered)
                        throw IllegalArgumentException("Unknown type of the answered task")


                    val checkedAnswers = answeredTask.answers.map {
                        if (it in properlyAnsweredTask.answers) it as Answer to true
                        else it as Answer to false
                    }
                        .toMap(LinkedHashMap())

                    TestChecked(answeredTask.id, answeredTask.title,
                        answeredTask == properlyAnsweredTask, checkedAnswers)
                }

    open fun getTestTaskAnswered(taskId: Long): LiveData<TestAnswered> =
        picMatchCacheSource.getPicMatchTestAnswered(taskId).map { it }
}