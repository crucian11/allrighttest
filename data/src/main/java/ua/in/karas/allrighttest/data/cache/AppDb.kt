package ua.`in`.karas.allrighttest.data.cache

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import ua.`in`.karas.allrighttest.data.cache.dao.TestsDao
import ua.`in`.karas.allrighttest.data.cache.dao.VariantsDao
import ua.`in`.karas.allrighttest.data.cache.model.entity.PicMatchTestAnswerEntity
import ua.`in`.karas.allrighttest.data.cache.model.entity.PicMatchTestEntity
import ua.`in`.karas.allrighttest.data.cache.model.entity.PicVariantEntity
import ua.`in`.karas.allrighttest.data.cache.model.entity.WordVariantEntity
import java.util.concurrent.Executors

@Database(
    version = 1,
    entities = [PicMatchTestEntity::class, PicVariantEntity::class, WordVariantEntity::class,
                        PicMatchTestAnswerEntity::class]
)
abstract class AppDb : RoomDatabase() {

    abstract fun testsDao(): TestsDao

    abstract fun variantsDao(): VariantsDao

    companion object {
        const val DB_NAME = "AllRightTestDB"

        private var INSTANCE: AppDb? = null

        @Synchronized
        fun instance(ctx: Context): AppDb {
            if (INSTANCE == null) {
                INSTANCE = createDb(ctx)
            }
            return INSTANCE!!
        }


        fun createDb(ctx: Context): AppDb =
            Room.databaseBuilder(ctx, AppDb::class.java, DB_NAME)
                .fallbackToDestructiveMigration()
                .addCallback(object : Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)

                        Executors.newSingleThreadExecutor().execute {

                            with(instance(ctx).variantsDao()) {
                                insertPicVariants(initialPicVariants)
                                insertWordVariants(initialWordVariants)
                            }

                            with(instance(ctx).testsDao()) {
                                insertPicMatchTests(initialTests)
                                insertPicMatchAnswers(initialTestAnswers)
                            }
                        }
                    }
                })
                .build()

        private const val assetsPath = "file:///android_asset/"

        private val initialPicVariants = listOf(
            PicVariantEntity(1, assetsPath + "aachoo.jpg"),
            PicVariantEntity(2, assetsPath + "cought.jpg"),
            PicVariantEntity(3, assetsPath + "temperature.jpg"),
            PicVariantEntity(4, assetsPath + "headache.jpg"),
            PicVariantEntity(5, assetsPath + "stomachache.jpg"),
            PicVariantEntity(6, assetsPath + "toothache.jpg"),
            PicVariantEntity(7, assetsPath + "earache.jpg"),
            PicVariantEntity(8, assetsPath + "backache.jpg")
        )

        private val initialWordVariants = listOf(
            WordVariantEntity(1, "a cold"),
            WordVariantEntity(2, "a cought"),
            WordVariantEntity(3, "a temperature"),
            WordVariantEntity(4, "a headache"),
            WordVariantEntity(5, "a stomach-ache"),
            WordVariantEntity(6, "a toothache"),
            WordVariantEntity(7, "an earache"),
            WordVariantEntity(8, "a backache")
        )

        private val initialTests = listOf(
            PicMatchTestEntity(1, "Look and match.")
        )

        private val initialTestAnswers = listOf(
            PicMatchTestAnswerEntity(1, 1, 1, 1),
            PicMatchTestAnswerEntity(1, 2, 2, 2),
            PicMatchTestAnswerEntity(1, 3, 3, 3),
            PicMatchTestAnswerEntity(1, 4, 4, 4),
            PicMatchTestAnswerEntity(1, 5, 5, 5),
            PicMatchTestAnswerEntity(1, 6, 6, 6),
            PicMatchTestAnswerEntity(1, 7, 7, 7),
            PicMatchTestAnswerEntity(1, 8, 8, 8)
        )

    }
}