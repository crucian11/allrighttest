package ua.`in`.karas.allrighttest.data.cache

import ua.`in`.karas.allrighttest.data.cache.model.PicMatchAnswerCache
import ua.`in`.karas.allrighttest.data.cache.model.PicMatchTestCache
import ua.`in`.karas.allrighttest.data.cache.model.entity.PicVariantEntity
import ua.`in`.karas.allrighttest.data.cache.model.entity.WordVariantEntity
import ua.`in`.karas.allrighttest.data.model.*

open class CacheMapper() {

    open fun mapPicMatchTestAnswered(matchTestCache: PicMatchTestCache): PicMatchTestAnswered =
            matchTestCache.run {
                PicMatchTestAnswered(
                    testEntity.id,
                    testEntity.title,
                    // Yeah, I know that sorting should be done in the database
                    // but @Relation doesn't have such functionality
                    // and creating own methods in the Dao is too verbose (especially if we want save Observable properties)
                    sortAnswers(answers).map(::mapPicMatchAnswer).toSet()
                )
            }



    open fun mapPicMatchTest(matchTestCache: PicMatchTestCache, shuffleMatchVariants: Boolean = false): PicMatchTest =
        matchTestCache.run {

            val picVariants = sortAnswers(answers).map { mapPicVariant(it.picVariantEntity.first()) }

            val matchVariants = sortAnswers(answers).map { mapWordVariant(it.wordVariantEntity.first()) }.let {
                if (shuffleMatchVariants) it.shuffled() else it
            }


            PicMatchTest(testEntity.id, testEntity.title, picVariants, matchVariants)
        }


    open fun mapPicMatchAnswer(answerCache: PicMatchAnswerCache): PicMatchAnswer =
            PicMatchAnswer(
                mapPicVariant(answerCache.picVariantEntity.first()),
                mapWordVariant(answerCache.wordVariantEntity.first())
            )

    open fun mapPicVariant(picVariantEntity: PicVariantEntity): PicVariant =
        picVariantEntity.run { PicVariant(id, picPath) }

    open fun mapWordVariant(wordVariantEntity: WordVariantEntity): WordVariant =
            wordVariantEntity.run { WordVariant(id, word) }


    private fun sortAnswers(answers: Set<PicMatchAnswerCache>) =
        answers.sortedBy { it.answerEntity.ordinal }

}