package ua.`in`.karas.allrighttest.data.cache

import androidx.lifecycle.LiveData
import ua.`in`.karas.allrighttest.data.cache.dao.TestsDao
import ua.`in`.karas.allrighttest.data.util.map
import ua.`in`.karas.allrighttest.data.model.PicMatchTest
import ua.`in`.karas.allrighttest.data.model.PicMatchTestAnswered
import ua.`in`.karas.allrighttest.data.util.nonNull

open class PicMatchCacheSource(private val testsDao: TestsDao, private val cacheMapper: CacheMapper) {

    open fun getPicMatchTest(taskId: Long): LiveData<PicMatchTest> =
        testsDao.getPicMatchTest(taskId).nonNull().map { cacheMapper.mapPicMatchTest(it, true) }

    open fun getPicMatchTestAnswered(taskId: Long): LiveData<PicMatchTestAnswered> =
        testsDao.getPicMatchTest(taskId).nonNull().map(cacheMapper::mapPicMatchTestAnswered)

}