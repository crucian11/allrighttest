package ua.`in`.karas.allrighttest.data.cache.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import ua.`in`.karas.allrighttest.data.cache.model.PicMatchTestCache
import ua.`in`.karas.allrighttest.data.cache.model.entity.PicMatchTestAnswerEntity
import ua.`in`.karas.allrighttest.data.cache.model.entity.PicMatchTestEntity

@Dao
interface TestsDao {

    @Insert
    fun insertPicMatchTests(tests: List<PicMatchTestEntity>)

    @Insert
    fun insertPicMatchAnswers(answers: List<PicMatchTestAnswerEntity>)

    @Transaction
    @Query("SELECT * FROM pic_match_test WHERE id = :id LIMIT 1")
    fun getPicMatchTest(id: Long = 1): LiveData<PicMatchTestCache>
}