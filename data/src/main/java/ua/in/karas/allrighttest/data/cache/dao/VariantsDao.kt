package ua.`in`.karas.allrighttest.data.cache.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import ua.`in`.karas.allrighttest.data.cache.model.entity.PicVariantEntity
import ua.`in`.karas.allrighttest.data.cache.model.entity.WordVariantEntity

@Dao
interface VariantsDao {

    @Insert
    fun insertPicVariants(picVariants: List<PicVariantEntity>)

    @Insert
    fun insertWordVariants(wordVariants: List<WordVariantEntity>)
}