package ua.`in`.karas.allrighttest.data.cache.model

import androidx.room.Embedded
import androidx.room.Relation
import ua.`in`.karas.allrighttest.data.cache.model.entity.PicMatchTestAnswerEntity
import ua.`in`.karas.allrighttest.data.cache.model.entity.PicVariantEntity
import ua.`in`.karas.allrighttest.data.cache.model.entity.WordVariantEntity

class PicMatchAnswerCache {
    @Embedded
    lateinit var answerEntity: PicMatchTestAnswerEntity

    @Relation(entity = PicVariantEntity::class, parentColumn = "picVariantId", entityColumn = "id")
    lateinit var picVariantEntity: List<PicVariantEntity>

    @Relation(entity = WordVariantEntity::class, parentColumn = "wordVariantId", entityColumn = "id")
    lateinit var wordVariantEntity: List<WordVariantEntity>

}