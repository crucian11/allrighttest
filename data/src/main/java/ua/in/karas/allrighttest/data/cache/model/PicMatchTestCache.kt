package ua.`in`.karas.allrighttest.data.cache.model

import androidx.room.Embedded
import androidx.room.Relation
import ua.`in`.karas.allrighttest.data.cache.model.entity.PicMatchTestAnswerEntity
import ua.`in`.karas.allrighttest.data.cache.model.entity.PicMatchTestEntity

class PicMatchTestCache {
    @Embedded
    lateinit var testEntity: PicMatchTestEntity

    @Relation(entity = PicMatchTestAnswerEntity::class, parentColumn = "id", entityColumn = "testId")
    lateinit var answers: Set<PicMatchAnswerCache>
}