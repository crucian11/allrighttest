package ua.`in`.karas.allrighttest.data.cache.model.entity

import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    tableName = "pic_match_test_answer",
    primaryKeys = ["testId", "picVariantId", "wordVariantId"],
    foreignKeys = [
        ForeignKey(entity = PicMatchTestEntity::class, parentColumns = ["id"], childColumns = ["testId"]),
        ForeignKey(entity = PicVariantEntity::class, parentColumns = ["id"], childColumns = ["picVariantId"]),
        ForeignKey(entity = WordVariantEntity::class, parentColumns = ["id"], childColumns = ["wordVariantId"])
    ]
)
data class PicMatchTestAnswerEntity(val testId: Long, val picVariantId: Long, val wordVariantId: Long, val ordinal: Int)