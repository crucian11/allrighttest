package ua.`in`.karas.allrighttest.data.cache.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "pic_match_test")
data class PicMatchTestEntity(@PrimaryKey val id: Long, val title: String)