package ua.`in`.karas.allrighttest.data.cache.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "pic_variant")
data class PicVariantEntity(@PrimaryKey val id: Long, val picPath: String)