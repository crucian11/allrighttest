package ua.`in`.karas.allrighttest.data.cache.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "word_variant")
data class WordVariantEntity(@PrimaryKey val id: Long, val word: String)