package ua.`in`.karas.allrighttest.data.model

sealed class Answer {
    abstract val variant: Variant
}

data class PicMatchAnswer(override val variant: PicVariant, val matchVariant: WordVariant): Answer()
