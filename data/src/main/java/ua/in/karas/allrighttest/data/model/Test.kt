package ua.`in`.karas.allrighttest.data.model

/***
 * The class, that represents a Test without any info about its correct answers
 * */
sealed class Test : TestBase {
    abstract override val id: Long
    abstract override val title: String
    abstract val variants: List<Variant>
}

data class PicMatchTest(override val id: Long, override val title: String, override val variants: List<PicVariant>,
                        val matchVariants: List<WordVariant>) : Test()