package ua.`in`.karas.allrighttest.data.model

/***
 * The class, that represents a Test with a set of Answer objects.
 * The [answers] property is not always represents proper solution.
 * */
sealed class TestAnswered : TestBase {
    abstract override val id: Long
    abstract override val title: String
    abstract val answers: Set<Answer>
}

data class PicMatchTestAnswered(override val id: Long, override val title: String,
                                override val answers: Set<PicMatchAnswer>) : TestAnswered()