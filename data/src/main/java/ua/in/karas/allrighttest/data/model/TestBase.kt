package ua.`in`.karas.allrighttest.data.model

interface TestBase {
    val id: Long
    val title: String
}