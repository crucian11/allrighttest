package ua.`in`.karas.allrighttest.data.model

/***
 * The class, that represents a Test check result.
 * The [checkedAnswers] property contains info about every answer.
 * */
data class TestChecked(override val id: Long, override val title: String,
                       val answeredProperly: Boolean, val checkedAnswers: Map<Answer, Boolean>): TestBase