package ua.`in`.karas.allrighttest.data.model

sealed class Variant

data class WordVariant(val id: Long, val word: String) : Variant()

data class PicVariant(val id: Long, val picPath: String) : Variant()