package ua.`in`.karas.allrighttest.data.util

import androidx.lifecycle.*

fun <T> LiveData<T>.observe(owner: LifecycleOwner, observer: (t: T?) -> Unit) {
    observe(owner, Observer { observer.invoke(it) })
}

fun <T> LiveData<T>.nonNull(): MediatorLiveData<T> {
    val mediator = MediatorLiveData<T>()
    mediator.addSource(this) { it?.let { mediator.value = it } }
    return mediator
}

fun <X, Y> LiveData<X>.map(func: (X) -> Y): LiveData<Y> =
    Transformations.map(this, func)

fun <X, Y> LiveData<X>.switchMap(func: (X) -> LiveData<Y>): LiveData<Y> =
    Transformations.switchMap(this, func)

fun <T, S> MediatorLiveData<T>.addSourceNonNull(source: LiveData<S>, onChanged: ((S) -> Unit)) {
    addSource(source){ it?.let(onChanged) }
}

