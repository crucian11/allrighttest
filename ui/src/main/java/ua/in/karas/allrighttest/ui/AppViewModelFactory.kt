package ua.`in`.karas.allrighttest.ui

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ua.`in`.karas.allrighttest.data.TestTaskRepo
import ua.`in`.karas.allrighttest.data.cache.AppDb
import ua.`in`.karas.allrighttest.data.cache.CacheMapper
import ua.`in`.karas.allrighttest.data.cache.PicMatchCacheSource
import ua.`in`.karas.allrighttest.ui.main.PickMatchTestViewModel
import ua.`in`.karas.allrighttest.ui.main.list.PicMatchListItemMapper

// All of dirty DI work is done here
class AppViewModelFactory(private val app: Application): ViewModelProvider.AndroidViewModelFactory(app) {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        when(modelClass) {
            PickMatchTestViewModel::class.java ->
                PickMatchTestViewModel(makeTestTaskRepo(), PicMatchListItemMapper()) as T

            else -> super.create(modelClass)
        }

    private fun makeTestTaskRepo() = TestTaskRepo(PicMatchCacheSource(AppDb.instance(app).testsDao(), CacheMapper()))
}