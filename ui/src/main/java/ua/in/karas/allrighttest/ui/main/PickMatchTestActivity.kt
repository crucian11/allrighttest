package ua.`in`.karas.allrighttest.ui.main

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.*
import com.google.android.material.snackbar.Snackbar
import ua.`in`.karas.allrighttest.ui.*
import ua.`in`.karas.allrighttest.ui.databinding.ActivityPickMatchTestBinding
import ua.`in`.karas.allrighttest.ui.databinding.ItemPicMatchTestWordBinding
import ua.`in`.karas.allrighttest.ui.main.list.PicMatchAdapter
import ua.`in`.karas.allrighttest.ui.main.list.PicMatchListItemDiffCB
import ua.`in`.karas.allrighttest.ui.main.model.PicMatchListItem
import ua.`in`.karas.allrighttest.ui.main.model.TestScreenState
import ua.`in`.karas.allrighttest.ui.util.*

class PickMatchTestActivity : AppCompatActivity() {

    private lateinit var vm: PickMatchTestViewModel
    private lateinit var binding: ActivityPickMatchTestBinding

    private val vmFactory: ViewModelProvider.Factory by lazy {
        AppViewModelFactory(application)
    }

    private val testAdapter by lazy { PicMatchAdapter() }

    private val layoutManager by lazy {
        GridLayoutManager(this, 2).apply {
            spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int =
                    if (testAdapter.items[position] is PicMatchListItem.Title) 2 else 1
            }
        }
    }

    private val retryMenuItem by lazy {
        binding.toolbarPickMatchTest.menu.findItem(R.id.item_pic_match_test_retry)
    }

    private val applyMenuItem by lazy {
        binding.toolbarPickMatchTest.menu.findItem(R.id.item_pic_match_test_apply)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            setTheme(R.style.AppTheme_LightStatusBar)
        }

        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_pick_match_test)

        with(binding) {
            setLifecycleOwner(this@PickMatchTestActivity)

            recyclerViewPickMatchTest.let { rv ->
                rv.adapter = testAdapter
                rv.layoutManager = layoutManager

                rv.addItemDecoration(
                    SpacesItemDecoration(resources.getDimensionPixelOffset(R.dimen.spacing_pico))
                )

                // ItemAnimator causes item's image blinking, better disable it at all
                rv.itemAnimator = null

                ItemTouchHelper(ItemTouchHelperCallback()).attachToRecyclerView(rv)
            }

            toolbarPickMatchTest.let { tb ->
                tb.inflateMenu(R.menu.menu_pick_match_test)
                tb.setOnMenuItemClickListener {
                    when (it.itemId) {
                        R.id.item_pic_match_test_retry -> vm.onRetryButtonClick()
                        R.id.item_pic_match_test_apply -> vm.onApplyButtonClick()
                    }

                    true
                }
            }
        }

        vm = withViewModel(vmFactory) {
            observeNotNull(testItems) {
                if (it != testAdapter.items) testAdapter.items = it
            }

            observeNotNull(screenState) {
                when (it) {
                    is TestScreenState.Default -> {
                        applyMenuItem.isVisible = true
                        retryMenuItem.isVisible = false
                    }

                    else -> {
                        applyMenuItem.isVisible = false
                        retryMenuItem.isVisible = true
                    }
                }
            }

            observe(showAnswersCorrectSnackbar) { showAnswersCorrectSnackbar() }
            observeNotNull(showAnswersWrongSnackbar) { showAnswersWrongSnackbar(it) }
        }
    }

    private fun showAnswersWrongSnackbar(addShowAnswersAction: Boolean) {
        Snackbar.make(binding.root, R.string.pic_match_test_your_answers_wrong_message, Snackbar.LENGTH_LONG).apply {
            setActionTextColor(colorAttr(R.attr.colorTextAnswerMessage))
            view.setBackgroundColor(colorAttr(R.attr.colorBgAnswerWrongMessage))

            if (addShowAnswersAction) setAction(R.string.pic_match_test_show_answer) { vm.onShowAnswerClick() }

            show()
        }

    }

    private fun showAnswersCorrectSnackbar() {
        Snackbar.make(binding.root, R.string.pic_match_test_your_answers_correct_message,  Snackbar.LENGTH_LONG).apply {
            setActionTextColor(colorAttr(R.attr.colorTextAnswerMessage))
            view.setBackgroundColor(colorAttr(R.attr.colorBgAnswerCorrectMessage))
        }
            .show()
    }


    private inner class ItemTouchHelperCallback
        : ItemTouchHelper.Callback() {

        override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int =
            if (isProperViewHolder(viewHolder) && vm.screenState.value is TestScreenState.Default) {
                makeMovementFlags(ItemTouchHelper.UP or ItemTouchHelper.DOWN, 0)
            }
            else 0

        override fun isLongPressDragEnabled(): Boolean = true

        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
                            target: RecyclerView.ViewHolder): Boolean {

            if (isProperViewHolder(target) && vm.screenState.value is TestScreenState.Default) {
                val oldPos = viewHolder.adapterPosition
                val newPos = target.adapterPosition

                testAdapter.onItemMove(oldPos, newPos)
                vm.onItemMove(oldPos, newPos)

                return true
            }
            else return false
        }

        private fun isProperViewHolder(target: RecyclerView.ViewHolder) =
            target is BindingViewHolder && target.binding is ItemPicMatchTestWordBinding

        override fun isItemViewSwipeEnabled(): Boolean = false

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {}
    }
}
