package ua.`in`.karas.allrighttest.ui.main

import androidx.lifecycle.*
import ua.`in`.karas.allrighttest.data.TestTaskRepo
import ua.`in`.karas.allrighttest.data.model.*
import ua.`in`.karas.allrighttest.ui.util.SingleLiveEvent
import ua.`in`.karas.allrighttest.data.util.addSourceNonNull
import ua.`in`.karas.allrighttest.data.util.map
import ua.`in`.karas.allrighttest.data.util.switchMap
import ua.`in`.karas.allrighttest.ui.main.list.PicMatchListItemMapper
import ua.`in`.karas.allrighttest.ui.main.model.PicMatchListItem
import ua.`in`.karas.allrighttest.ui.main.model.TestScreenState
import java.util.*

class PickMatchTestViewModel(
    private val testsRepo: TestTaskRepo,
    private val itemFactory: PicMatchListItemMapper
) : ViewModel() {

    private var failuresCount: Int = 0

    private val _screenState = MutableLiveData<TestScreenState>()
    val screenState: LiveData<TestScreenState>
            get() = _screenState

    private val _testItems = MediatorLiveData<List<PicMatchListItem>>()
    val testItems: LiveData<List<PicMatchListItem>>
        get() = _testItems

    private val _showAnswersWrongSnackbar = SingleLiveEvent<Boolean>()
    val showAnswersWrongSnackbar: LiveData<Boolean>
        get() = _showAnswersWrongSnackbar

    private val _showAnswersCorrectSnackbar = SingleLiveEvent<Unit>()
    val showAnswersCorrectSnackbar: LiveData<Unit>
        get() = _showAnswersCorrectSnackbar


    init {
        _screenState.value = TestScreenState.Default(TEST_ID)

        val dataSource = _screenState.switchMap { state ->
            when (state) {
                is TestScreenState.Default ->
                    testsRepo.getTaskTask(state.testId)
                        .map { itemFactory.mapToListItems(it as PicMatchTest) }

                is TestScreenState.Checked ->
                    testsRepo.checkAnswers(state.answeredTest)
                        .map {

                            // Forced to do it there, LiveData doesn't contain superb-convenient doOnNext from RxJava
                            if (it.answeredProperly)
                                _showAnswersCorrectSnackbar.postValue(null)
                            else
                                _showAnswersWrongSnackbar.postValue(++failuresCount > BUTTON_SHOW_ANSWER_FAILURES_COUNT)


                            itemFactory.mapToListItems(it)
                        }

                is TestScreenState.AnswersShown ->
                    testsRepo.getTestTaskAnswered(state.testId)
                        .map { itemFactory.mapToListItems(it as PicMatchTestAnswered) }
            }
        }

        _testItems.addSourceNonNull(dataSource, _testItems::setValue)
    }


    fun onApplyButtonClick() {
        _testItems.value?.let { _screenState.value = TestScreenState.Checked(itemFactory.mapFromListItems(it)) }
    }

    fun onRetryButtonClick() {
        _screenState.value = TestScreenState.Default(TEST_ID)
    }

    fun onShowAnswerClick() {
        (_screenState.value as? TestScreenState.Checked)?.let {
            _screenState.value = TestScreenState.AnswersShown(it.answeredTest.id)
        }
    }

    fun onItemMove(oldPos: Int, newPos: Int) {
        val items = _testItems.value?.toMutableList()

        if (items != null) {
            if (_screenState.value !is TestScreenState.Default)
                throw IllegalStateException("Items can be moved only in default TestScreenState")

            if (items[oldPos] !is PicMatchListItem.WordToMatch || items[newPos] !is PicMatchListItem.WordToMatch)
                throw IllegalArgumentException("Only PicMatchListItem.WordToMatch items can be moved on this screen")

            _testItems.value = items.also { Collections.swap(it, oldPos, newPos) }
        }

    }

    companion object {
        private const val TEST_ID = 1L

        private const val BUTTON_SHOW_ANSWER_FAILURES_COUNT = 2
    }

}