package ua.`in`.karas.allrighttest.ui.main.list

import android.util.Log
import androidx.recyclerview.widget.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ua.`in`.karas.allrighttest.ui.R
import ua.`in`.karas.allrighttest.ui.util.BindingViewHolder
import ua.`in`.karas.allrighttest.ui.main.model.PicMatchListItem
import java.util.*

class PicMatchAdapter: RecyclerView.Adapter<BindingViewHolder>() {

    // TODO: Calculate the DiffResult in background. ListAdapter is not applicable due to ItemTouchHelper
    var items: List<PicMatchListItem>
        get() = _items.toList()
        set(value) {
            DiffUtil.calculateDiff(PicMatchListItemDiffCB(items, value)).run {
                _items = value.toMutableList()
                dispatchUpdatesTo(this@PicMatchAdapter)
            }
        }

    private var _items: MutableList<PicMatchListItem> = mutableListOf()


    override fun getItemCount(): Int = _items.size

    override fun getItemViewType(position: Int): Int =
            when (_items[position]) {
                is PicMatchListItem.Title -> R.layout.item_pic_match_test_title
                is PicMatchListItem.Pic -> R.layout.item_pic_match_test_picture
                is PicMatchListItem.WordToMatch -> R.layout.item_pic_match_test_word
            }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder =
        BindingViewHolder(
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), viewType, parent, false)
        )


    override fun onBindViewHolder(holder: BindingViewHolder, position: Int) {
        Log.d("PicMatchAdapter onBind", "position: $position")
        holder.bind(_items[position])
    }



    fun onItemMove(oldPos: Int, newPos: Int) {
        Log.d("PicMatchAdapter", "oldPos: $oldPos ; newPos: $newPos")
        Collections.swap(_items, oldPos, newPos)
        //notifyItemChanged(oldPos)
        //notifyItemChanged(newPos)
        notifyItemMoved(oldPos, newPos)
    }

}