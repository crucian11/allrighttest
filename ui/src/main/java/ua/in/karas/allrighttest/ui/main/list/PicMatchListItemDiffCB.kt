package ua.`in`.karas.allrighttest.ui.main.list

import androidx.recyclerview.widget.DiffUtil
import ua.`in`.karas.allrighttest.ui.main.model.*

class PicMatchListItemDiffCB(private val oldList: List<PicMatchListItem>,
                              private val newList: List<PicMatchListItem>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        return when {
            oldItem is PicMatchListItem.Title && newItem is PicMatchListItem.Title ->
                oldItem.testId == newItem.testId

            oldItem is PicMatchListItem.Pic && newItem is PicMatchListItem.Pic ->
                oldItem.picVariant.id == newItem.picVariant.id

            oldItem is PicMatchListItem.WordToMatch && newItem is PicMatchListItem.WordToMatch ->
                oldItem.wordVariant.id == newItem.wordVariant.id

            else -> false
        }
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition] == newList[newItemPosition]

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size
}