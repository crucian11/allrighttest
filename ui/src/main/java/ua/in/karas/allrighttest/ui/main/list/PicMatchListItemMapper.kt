package ua.`in`.karas.allrighttest.ui.main.list

import ua.`in`.karas.allrighttest.data.model.*
import ua.`in`.karas.allrighttest.ui.main.model.TestListItemState
import ua.`in`.karas.allrighttest.ui.main.model.PicMatchListItem

// Convert complex objects to a flatten list to make an adapter as dump as possible
open class PicMatchListItemMapper {

    open fun mapToListItems(test: PicMatchTest): List<PicMatchListItem> =
        test.variants.zip(test.matchVariants)
            .foldIndexed(initialList(test.id, test.title)) { i, acc, (variant, matchVariant) ->
                acc.apply {
                    add(PicMatchListItem.Pic(i + 1, variant, TestListItemState.DEFAULT))
                    add(PicMatchListItem.WordToMatch(matchVariant, TestListItemState.DEFAULT))
                }
            }

    open fun mapToListItems(checkedTest: TestChecked): List<PicMatchListItem> =
        checkedTest.checkedAnswers
            .toList()
            .map { (answer, correct) ->
                answer as PicMatchAnswer to if (correct) TestListItemState.CORRECT else TestListItemState.WRONG
            }
            .foldIndexed(initialList(checkedTest.id, checkedTest.title)) { i, acc, (answer, itemStatus) ->
                acc.apply {
                    add(PicMatchListItem.Pic(i + 1, answer.variant, itemStatus))
                    add(PicMatchListItem.WordToMatch(answer.matchVariant, itemStatus))
                }
            }

    open fun mapToListItems(answeredTest: PicMatchTestAnswered): List<PicMatchListItem> =
        answeredTest.answers
            .foldIndexed(initialList(answeredTest.id, answeredTest.title)) { i, acc, answer ->
                acc.apply {
                    add(PicMatchListItem.Pic(i + 1, answer.variant, TestListItemState.ANSWERS_SHOWN))
                    add(PicMatchListItem.WordToMatch(answer.matchVariant, TestListItemState.ANSWERS_SHOWN))
                }
            }


    open fun mapFromListItems(items: List<PicMatchListItem>): PicMatchTestAnswered {
        val (testId, title) = items[0] as PicMatchListItem.Title

        val answers =
            (1 until items.size step 2)
                .map { i ->
                    items[i] as PicMatchListItem.Pic to items[i + 1] as PicMatchListItem.WordToMatch
                }
                .map { (picItem, wordItem) ->
                    PicMatchAnswer(picItem.picVariant, wordItem.wordVariant)
                }
                .toSet()

        return PicMatchTestAnswered(testId, title, answers)
    }


    private fun initialList(id: Long, title: String) =
        mutableListOf<PicMatchListItem>(PicMatchListItem.Title(id, title))
}