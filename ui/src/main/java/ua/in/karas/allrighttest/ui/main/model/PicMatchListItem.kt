package ua.`in`.karas.allrighttest.ui.main.model

import androidx.annotation.AttrRes
import ua.`in`.karas.allrighttest.data.model.PicVariant
import ua.`in`.karas.allrighttest.data.model.WordVariant
import ua.`in`.karas.allrighttest.ui.R

sealed class PicMatchListItem {

    data class Title(val testId: Long, val title: String) : PicMatchListItem()

    data class Pic(val number: Int, val picVariant: PicVariant, val state: TestListItemState): PicMatchListItem() {
        val numberStr: String
                get() = number.toString()

        @AttrRes
        val stateColorThemeAttr: Int =
            when (state) {
                TestListItemState.DEFAULT -> R.attr.colorAnswerDefault
                TestListItemState.CORRECT -> R.attr.colorAnswerCorrect
                TestListItemState.WRONG -> R.attr.colorAnswerWrong
                TestListItemState.ANSWERS_SHOWN -> R.attr.colorAnswerShown
            }

    }

    data class WordToMatch(val wordVariant: WordVariant, val state: TestListItemState): PicMatchListItem()
}

