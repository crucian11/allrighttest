package ua.`in`.karas.allrighttest.ui.main.model

enum class TestListItemState {
    DEFAULT, CORRECT, WRONG, ANSWERS_SHOWN
}