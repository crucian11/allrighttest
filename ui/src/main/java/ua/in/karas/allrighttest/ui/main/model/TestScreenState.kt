package ua.`in`.karas.allrighttest.ui.main.model

import ua.`in`.karas.allrighttest.data.model.TestAnswered

sealed class TestScreenState {
    data class Default(val testId: Long): TestScreenState()
    data class Checked(val answeredTest: TestAnswered): TestScreenState()
    data class AnswersShown(val testId: Long): TestScreenState()
}