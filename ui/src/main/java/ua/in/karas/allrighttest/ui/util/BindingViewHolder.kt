package ua.`in`.karas.allrighttest.ui.util

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import ua.`in`.karas.allrighttest.ui.BR

open class BindingViewHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

    open fun bind(itemModel: Any?) {
        with(binding) {
            setVariable(BR.itemModel, itemModel)
            executePendingBindings()
        }
    }
}

