package ua.`in`.karas.allrighttest.ui.util

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.*
import androidx.lifecycle.ViewModelProviders
import ua.`in`.karas.allrighttest.data.util.observe


fun <T : Any, L : LiveData<T>> LifecycleOwner.observe(liveData: L, body: (T?) -> Unit) {
    liveData.observe(this, body)
}

fun <T : Any, L : LiveData<T>> LifecycleOwner.observeNotNull(liveData: L, body: (T) -> Unit) {
    liveData.observe(this) { value -> value?.let { body(it) } }
}

inline fun <reified T : ViewModel> AppCompatActivity.getViewModel(
    factory: ViewModelProvider.Factory? = null
): T {

    return factory?.let { ViewModelProviders.of(this, it)[T::class.java] }
        ?: ViewModelProviders.of(this)[T::class.java]
}

inline fun <reified T : ViewModel> AppCompatActivity.withViewModel(
    factory: ViewModelProvider.Factory? = null,
    body: T.() -> Unit
): T {

    val vm = getViewModel<T>(factory)
    vm.body()
    return vm
}
