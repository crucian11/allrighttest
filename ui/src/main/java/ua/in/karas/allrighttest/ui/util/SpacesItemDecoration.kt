package ua.`in`.karas.allrighttest.ui.util

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class SpacesItemDecoration(private val space: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        with(outRect) {
            left = space
            right = space
            bottom = space
        }

        // Add top margin only for the first item to avoid double space between items
        if (parent.getChildLayoutPosition(view) === 0) {
            outRect.top = space
        } else {
            outRect.top = 0
        }
    }
}