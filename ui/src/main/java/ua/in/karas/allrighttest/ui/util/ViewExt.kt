package ua.`in`.karas.allrighttest.ui.util

import android.content.Context
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import androidx.annotation.AttrRes
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter("loadImage")
fun ImageView.loadImage(imagePath: String) {
    Glide.with(this).load(imagePath).dontAnimate().into(this)
}

@BindingAdapter("backgroundColorFromAttr")
fun View.backgroundColorFromAttr(@AttrRes themeColorAttr: Int) {
    setBackgroundColor(context.colorAttr(themeColorAttr))
}

fun Context.colorAttr(@AttrRes colorAttr: Int): Int {
    val typedValue = TypedValue()
    theme.resolveAttribute(colorAttr, typedValue, true)
    return typedValue.data
}